﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using MathLibrary;
using Point = MathLibrary.Point;
using System.Windows.Forms;


namespace GeometryLibrary
{
    /// <summary>
    /// Class for lines in 3D space.
    /// </summary>
    public class Line : Curve
    {
        /// <summary>
        /// The start point of the line.
        /// </summary>
        public Point StartPoint { get; set; }

        /// <summary>
        /// The end point of the line.
        /// </summary>
        public Point EndPoint { get; set; }

        /// <summary>
        /// The length of the line.
        /// </summary>
        public override double Length => StartPoint.DistanceTo(EndPoint);

        /// <summary>
        /// The direction of the line as a normalized vector
        /// </summary>
        public Vector Direction => new Vector(EndPoint.X - StartPoint.X, EndPoint.Y - StartPoint.Y, EndPoint.Z - StartPoint.Z).Normalize();

        /// <summary>
        /// Draws a line.
        /// </summary>
        /// <param name="g">The graphics context to be used.</param>
        public override void Draw(Graphics g)
        {
            Pen aPen = new Pen(Color.Red, 3);
            g.DrawLine(aPen, (float)StartPoint.X, (float)StartPoint.Y, (float)EndPoint.X, (float)EndPoint.Y);
        }

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        public Line(Point startPoint, Point endPoint)
        {
            this.StartPoint = startPoint;
            this.EndPoint = endPoint;
        }

        //Pre-processing for MouseDown-Event.
        //Define the result of one mouse-down event when it happen, and evaluate it 
        //whether it belongs to first point, end point, in-progressing point of shape, or cancel the current process 
        public static ClickResult LineClickHandler(System.Drawing.Point pt, MouseButtons e, int screenHeight, ref Curve curElement, out string statusMessage)
        {
            statusMessage = StartMessage;
            if (e == MouseButtons.Right)
                return ClickResult.Canceled;

            //Point currentPoint = new Point(pt.X, pt.Y);
            Point currentPoint = TransformScreen2World(pt, screenHeight);

            if (curElement == null||curElement.GetType()!=typeof(Line))
            {
                curElement = new Line(currentPoint, currentPoint);
                statusMessage = EndMessage;
                return ClickResult.Created;
            }

            (curElement as Line).EndPoint = currentPoint;
            return ClickResult.Finished;
        }

        public const string StartMessage = "Please select the start point of the line.";

        public const string EndMessage = "Please select the end point of the line.";
    }
}
