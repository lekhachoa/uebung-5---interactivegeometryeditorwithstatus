﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using MathLibrary;
using Point = MathLibrary.Point;
using System.Windows.Forms;

namespace GeometryLibrary
{
    /// <summary>
    /// Class for circles in 3D space.
    /// </summary>
    public class Circle : Curve, ISurface
    {
        /// <summary>
        /// The center point of the circle.
        /// </summary>
        public Point CenterPoint { get; set; }

        /// <summary>
        /// The normal of the circle.
        /// </summary>
        public Vector Normal { get; set; }

        /// <summary>
        /// The radius of the circle.
        /// </summary>
        public double Radius { get; set; }

        /// <summary>
        /// The length of the circle.
        /// </summary>
        public override double Length => 2.0 * Math.PI * Radius;

        /// <summary>
        /// Draws a circle.
        /// </summary>
        /// <param name="g">The graphics context to be used.</param>
        public override void Draw(Graphics g)
        {
            Pen aPen = new Pen(Color.Green, 3);
            // Build a rectangle to describe the circle
            float x = (float)(CenterPoint.X - Radius);
            float y = (float)(CenterPoint.Y - Radius);
            float diameter = 2f * (float)Radius;
            RectangleF rectangle = new RectangleF(x, y, diameter, diameter);

            g.DrawEllipse(aPen, rectangle);
        }

        /// <summary>
        /// Creates a new instance.
        /// </summary>
        /// <param name="centerPoint">The center point.</param>
        /// <param name="normal">The normal.</param>
        /// <param name="radius">The radius.</param>
        public Circle(Point centerPoint, Vector normal, double radius)
        {
            this.CenterPoint = centerPoint;
            this.Normal = normal;
            this.Radius = radius;
        }

        /// <summary>
        /// The area of the circle.
        /// </summary>
        public double Area => Math.PI * Radius * Radius;

        public static ClickResult CircleClickHandler(System.Drawing.Point pt, MouseButtons e, int screenHeight, ref Curve curElement, out string statusMessage)
        {
            statusMessage = StartMessage;
            if (e == MouseButtons.Right)
                return ClickResult.Canceled;

            Point currentPoint = TransformScreen2World(pt, screenHeight);
            if (curElement == null || curElement.GetType() != typeof(Circle))
            {
                curElement = new Circle(currentPoint, Vector.Zero, 0);
                statusMessage = EndMessage;
                return ClickResult.Created;
            }

            (curElement as Circle).Radius = (curElement as Circle).CenterPoint.DistanceTo(currentPoint);
            return ClickResult.Finished;
        }

        public const string StartMessage = "Please select the center point of the circle.";

        public const string EndMessage = "Please select a point on radius of the circle.";
    }
}
