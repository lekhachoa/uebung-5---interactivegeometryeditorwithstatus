﻿using System;
using System.Drawing;
using System.Windows.Forms;
using GeometryLibrary;

namespace GeometryEditorApp
{
    public partial class MainForm : Form
    {
        private Graphics _pictureBoxGraphics;
        private Drawing _drawing = new Drawing(new Curve[0]);

        public MainForm()
        {
            InitializeComponent();
            //Create reverse Graphic for drawing on
            _pictureBoxGraphics = pictureBox.CreateGraphics();
            SetGraphicsTransformToWorld(_pictureBoxGraphics);

            _drawing.Redraw += OnRedraw;

            StatusManager.Instance.StatusMessageChanged += (o, e) => toolStripStatusLabel.Text = e.Message;
        }

        private void SetGraphicsTransformToWorld(Graphics g)
        {
            g.ResetTransform();
            g.ScaleTransform(1f, -1f);
            g.TranslateTransform(0f, -pictureBox.Height);
        }

        private CurveClickHandler _curveClickHandler = null;

        private Curve _currentCurve = null;

        #region DrawingButton
        //Press lineButton to draw line
        private void toolStripButtonLine_Click(object sender, EventArgs e)
        {
            //set variable _curveClickHander as LineClickHandler currently
            _curveClickHandler = Line.LineClickHandler;
            _currentCurve = null;
            StatusManager.Instance.SetStatus(Line.StartMessage);

            //Trigger Event - along with a message
            //StatusManager.Instance.SetStatus("Drawing line at the moment, Pls select a start point of line");

            //to set current button as checked and uncheck other buttons
            toolStripButtonLine.Checked = true;
            toolStripButtonCircle.Checked = false;
            toolStripButtonPolyline.Checked = false;
            //For support Line only
            drawingPressButton = sender;
        }

        private void toolStripButtonCircle_Click(object sender, EventArgs e)
        {
            _curveClickHandler = Circle.CircleClickHandler;

            //_currentCurve = null;
            StatusManager.Instance.SetStatus("Drawing circle at the moment, Pls select a center point of circle");
            toolStripButtonCircle.Checked = true;
            toolStripButtonLine.Checked = false;
            toolStripButtonPolyline.Checked = false;
            drawingPressButton = sender;
        }

        private void toolStripButtonPolyline_Click(object sender, EventArgs e)
        {
            _curveClickHandler = Polyline.PolylineClickHandler;

            //_currentCurve = null;
            StatusManager.Instance.SetStatus("Drawing polyline at the moment, Pls select a start point of polyline");
            toolStripButtonPolyline.CheckState = CheckState.Checked;
            toolStripButtonLine.CheckState = CheckState.Unchecked;
            toolStripButtonCircle.CheckState = CheckState.Unchecked;
            drawingPressButton = sender;
        }

        #endregion 

        #region Mouse Down Event
        //Define behavior after Clicking 
        //And those Evaluation are based on type of shape. (which was defined by button before)
        ClickResult clickResult;

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (_curveClickHandler == null)
                return;

            clickResult = _curveClickHandler(e.Location, e.Button, pictureBox.Height, ref _currentCurve, out string statusMessage);
            StatusManager.Instance.SetStatus(statusMessage); //Instead of trigger Event everytime at every Clickresult, we trigger it at first, ones for all

            if (clickResult == ClickResult.Canceled)
            {
                (drawingPressButton as ToolStripButton).Checked = false;
                _curveClickHandler = null;
                _currentCurve = null;
            }

            if (clickResult == ClickResult.Created)
            {
                Refresh();
            }

            if (clickResult == ClickResult.Finished)
            {
                _drawing.AddCurve(_currentCurve);
                _currentCurve = null;
                firstPressOnPictureBox = false;
            }
            //for support Line only
            firstPressOnPictureBox = true;
            lastMouseClick = e;
            if (clickResult == ClickResult.PointHandled)
            {
                (_currentCurve as Polyline).Draw(_pictureBoxGraphics);
            }
            
        }
        #endregion

        #region PainNRepaint
        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            SetGraphicsTransformToWorld(g);

            if (showLinesButton.Checked)
                _drawing.GetLines().DrawElements(g);

            if (showCirclesButton.Checked)
                _drawing.GetCircles().DrawElements(g);

            if (showPolylinesButton.Checked)
                _drawing.GetPolylines().DrawElements(g);
        }

        private void OnRedraw(Object sender, EventArgs e)
        {
            pictureBox.Invalidate();
            //_drawing.Draw(_pictureBoxGraphics); //not optimal
            //_drawing.Last.Draw(_pictureBoxGraphics); //Draw a specific Curve instead of all Curve-drawing-list
            //_drawing.GetLast().Draw(_pictureBoxGraphics);
        }

        private void showLinesButton_Click(object sender, EventArgs e)
        {
            pictureBox.Invalidate();
        }

        private void showCirclesButton_Click(object sender, EventArgs e)
        {
            pictureBox.Invalidate();
        }

        private void showPolylinesButton_Click(object sender, EventArgs e)
        {
            pictureBox.Invalidate();
        }

        private void pictureBox_Resize(object sender, EventArgs e)
        {
            pictureBox.Invalidate();
        }

        private void pictureBox_SizeChanged(object sender, EventArgs e)
        {
            //Update Graphic with new height of pictureBox
            _pictureBoxGraphics = pictureBox.CreateGraphics();
            SetGraphicsTransformToWorld(_pictureBoxGraphics);
        }
        #endregion

        #region my mouse move feature
        //for support Line only
        object drawingPressButton;
        Pen writingPen = new Pen(Color.Blue, 1);
        Pen erasingPen = new Pen(DefaultBackColor, 1);
        bool firstPressOnPictureBox = false;
        int currentX = 0, currentY = 0;
        MouseEventArgs lastMouseClick;

        //for private using only, to draw a support Line while moving mouse
        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (!firstPressOnPictureBox || drawingPressButton == null)
                return;

            Graphics g = pictureBox.CreateGraphics();
            if (drawingPressButton.ToString() == "Line")
                if (clickResult == ClickResult.Created)
                {
                    g.DrawLine(erasingPen, lastMouseClick.X, lastMouseClick.Y, currentX, currentY);
                    //Refresh();
                    currentX = e.X; currentY = e.Y;
                    g.DrawLine(writingPen, lastMouseClick.X, lastMouseClick.Y, currentX, currentY);
                }
            if (drawingPressButton.ToString() == "Circle")
                if (clickResult == ClickResult.Created)
                {
                    currentX = e.X; currentY = e.Y;
                    int radius = (int)Math.Sqrt(Math.Pow(lastMouseClick.X - currentX, 2) + Math.Pow(lastMouseClick.Y - currentY, 2));
                    g.DrawEllipse(erasingPen, lastMouseClick.X - radius, lastMouseClick.Y - radius, radius * 2, radius * 2);
                    Refresh();
                    g.DrawEllipse(writingPen, lastMouseClick.X - radius, lastMouseClick.Y - radius, radius * 2, radius * 2);
                }
            if (drawingPressButton.ToString() == "Polyline")
                if (clickResult == ClickResult.Created || clickResult == ClickResult.PointHandled)
                {
                    g.DrawLine(erasingPen, lastMouseClick.X, lastMouseClick.Y, currentX, currentY);
                    currentX = e.X; currentY = e.Y;
                    //Refresh();
                    g.DrawLine(writingPen, lastMouseClick.X, lastMouseClick.Y, currentX, currentY);
                }
        }
        #endregion 
    }
}
